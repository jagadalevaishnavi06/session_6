
document.getElementById("button1").addEventListener("click", function () {
    fetch('https://dummyjson.com/products')
    .then((response) => response.json())
    .then((json) => console.log(json));
});

document.getElementById("button2").addEventListener("click", function () {
    fetch('https://dummyjson.com/todos')
  .then((response) => response.json())
  .then((json) => console.log(json));
});

document.getElementById("button3").addEventListener("click", function () {
    fetch('https://dummyjson.com/users')
    .then((response) => response.json())
    .then((json) => console.log(json));
});

document.getElementById("button4").addEventListener("click", function () {
    fetch('https://dummyjson.com/comments')
  .then((response) => response.json())
  .then((json) => console.log(json));
});

document.getElementById("button5").addEventListener("click", function () {
    fetch('https://dummyjson.com/quotes')
  .then((response) => response.json())
  .then((json) => console.log(json));
});

